import { browser } from 'protractor';
import { By}  from 'protractor';
import { until } from 'protractor';

const auth_data = require('/home/leon/dev/red-hat-task/spec/your-store/auth.json');
const TEST_EXPECT = 'User login flow';
const TEST_DESCR = 'In order to test login flow we need to assure that after login is done, user information on account page contains proper email';

describe(TEST_DESCR, () => {
    const start_url = 'http://tutorialsninja.com/demo/';
    const HDR_ACC_MENU_XPTH = '//div[@id="top-links"]/ul[contains(@class,"list-inline")]/li[contains(@class,"dropdown")]';
    const LOGIN_MENU_ITEM_XPTH = '//ul[contains(@class,"dropdown-menu")]/li/a[contains(@href,"route=account/login")]';
    const USERNAME_INPUT_XPTH = '//input[@id="input-email"]';
    const USERNAME = 'youcanleave@your.hat.on';
    const PASSWORD_INPUT_XPTH = '//input[@id="input-password"]';
    const PASSWORD = 'YourHat0n\n';
    const MY_ACC_LINK_XPTH = '//a[contains(@href,"index.php?route=account/edit")]';

    it(TEST_EXPECT, () => {
        browser.driver.get(start_url).then(() => {
            browser.driver.sleep(2000);
            browser.driver.findElement(By.xpath(HDR_ACC_MENU_XPTH)).click().then( () => {

                browser.driver.sleep(500);
                browser.driver.findElement(By.xpath(LOGIN_MENU_ITEM_XPTH)).then((login_mi) => {

                    expect(until.elementIsVisible(login_mi));
                    login_mi.click().then( () => {
                        browser.driver.findElement(By.xpath(USERNAME_INPUT_XPTH)).sendKeys(auth_data.username).then( () => {

                            browser.driver.sleep(500);
                            browser.driver.findElement(By.xpath(PASSWORD_INPUT_XPTH)).sendKeys(auth_data.password + '\n').then( () => {

                                browser.driver.sleep(500);
                                browser.driver.findElement(By.xpath(MY_ACC_LINK_XPTH)).click().then( () => {

                                    browser.driver.sleep(500);
                                    let uname_inp = browser.driver.findElement(By.xpath(USERNAME_INPUT_XPTH));
                                    expect(until.elementTextIs(uname_inp, auth_data.username));
                                    console.debug('Login is successful!');
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
