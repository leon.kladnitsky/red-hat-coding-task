import { browser } from 'protractor';
import { By}  from 'protractor';
import { until } from 'protractor';
import assert = require("assert");

const TEST_EXPECT = 'Product sorting by price ';
const TEST_DESCR = 'In order to test sorting we need to assure that after the listing page is sorted, the array of prices (striped from it) will be properly sorted';

describe(TEST_DESCR, () => {
    const start_url = 'http://tutorialsninja.com/demo/index.php?route=product/category&path=24';
    const SORT_MENU_XPTH = '//select[@id="input-sort"]';
    const PRICE_ASC_OPT_XPTH = '//option[contains(@value, "&sort=p.price&order=ASC")]';
    const PRICE_DEC_OPT_XPTH = '//option[contains(@value, "&sort=p.price&order=DESC")]';
    const PRICE_TAG_XPTH = '//p[contains(@class,"price")]';

    it(TEST_EXPECT, () => {
        browser.driver.get(start_url).then(() => {

            browser.driver.sleep(2000);
            browser.driver.findElement(By.xpath(SORT_MENU_XPTH)).click().then( () => {

                browser.driver.sleep(500);
                browser.driver.findElement(By.xpath(PRICE_ASC_OPT_XPTH)).then( (asc_opt) => {

                    expect(until.elementIsVisible(asc_opt));
                    asc_opt.click().then( () => {
                        browser.driver.sleep(500);
                        let prices = [];
                        browser.driver.findElements(By.xpath(PRICE_TAG_XPTH)).then((prices_listed) => {

                            // debugger;
                            for (let pricesListedKey in prices_listed) {
                                prices.push(pricesListedKey);
                            }
                            assert(prices.length == prices_listed.length);
                        });
                        // let isSorted = arr => arr.every((v,i,a) => !i || a[i-1] <= v);
                        assert(prices.every((v,i,a) => !i || a[i-1] <= v));
                    });
                });

                browser.driver.findElement(By.xpath(PRICE_DEC_OPT_XPTH)).then( (dec_opt) => {

                    expect(until.elementIsVisible(dec_opt));
                    dec_opt.click().then( () => {
                        let prices = [];
                        browser.driver.findElements(By.xpath(PRICE_TAG_XPTH)).then((prices_listed) => {

                            // debugger;
                            for (let pricesListedKey in prices_listed) {
                                prices.push(pricesListedKey);
                            }
                            assert(prices.length == prices_listed.length);
                        });
                        // let isSorted = arr => arr.every((v,i,a) => !i || a[i-1] <= v);
                        assert(prices.every((v,i,a) => !i || a[i-1] >= v));
                    });
                });
            });
        });
    });
});
