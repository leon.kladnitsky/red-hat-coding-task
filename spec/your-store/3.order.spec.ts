import {browser, until} from 'protractor';
import { By}  from 'protractor';
import assert = require("assert");
// import {async} from "q";

const auth_data = require('/home/leon/dev/red-hat-task/spec/your-store/auth.json');
const order_data = require('/home/leon/dev/red-hat-task/spec/your-store/order.json');
const TEST_EXPECT = 'Placing an order';
const TEST_DESCR = 'Find order by id or name, then add it to card, then checkout';

describe(TEST_DESCR, () => {
    const SEARCH_XPTH = '//input[@placeholder="Search"]';
    const ADD2CART_BTN_XPTH = '//button[@id="button-cart"]';
    const ADDED_MSG_XPTH = '//div[contains(@class,"alert-success")]/a[contains(@href,"index.php?route=product/product&product_id={{product_id}}")]';
    const CHKOUT_LNK_XPTH = '//a[contains(@href, "index.php?route=checkout/checkout")]';
    const CHKOUT_1ST_STEP_XPATH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]';
    const USERNAME_INPUT_XPTH = '//input[@id="input-email"]';
    const PASSWORD_INPUT_XPTH = '//input[@id="input-password"]';

    const BILL_ADDR_EXIST_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//input[@value="existing"]';
    const BILL_ADDR_DD_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//select[@name="address_id"]/option[1]';
    const BILL_CONT_BTN_XPTH = '//input[@id="button-payment-address"]';

    const DEL_ADDR_EXIST_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//input[@value="existing"]';
    // const DEL_ADDR_DD_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//select[@name="address_id"]/option[1]';
    const DEL_CONT_BTN_XPTH = '//input[@id="button-shipping-address"]';

    const DEL_METH_EXIST_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//input[@value="flat.flat"]';
    const DEL_METH_BTN_XPTH = '//input[@id="button-shipping-method"]';

    const PAY_METH_EXIST_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//input[@value="cod"]';
    const PAY_METH_AGREE_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//input[@value="1"]';
    const PAY_METH_BTN_XPTH = '//input[@id="button-payment-method"]';

    const CONFIRM_ROW_PRICE_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//table/tbody/tr/td[last()]';
    const CONFIRM_ROW_SUBTT_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//table/tfoot/tr[1]/td[last()]';
    const CONFIRM_ROW_SHPNG_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//table/tfoot/tr[2]/td[last()]';
    const CONFIRM_ROW_TOTAL_XPTH = '//div[contains(@class, "panel-collapse") and contains(@class, "in")]//table/tfoot/tr[3]/td[last()]';

    const CONFIRM_BTN_XPTH = '//input[@id="button-confirm"]';

    const ORDER_SUCSESS_MSG_XPTH = '//div[@id="content"]/h1[1]';
    const ORDER_SUCSESS_BTN_XPTH = '//a[contains(@href,"http://tutorialsninja.com/demo/index.php?route=common/home")]';


    it(TEST_EXPECT, () => {

        // browser.waitForAngularEnabled(false);
        // let originalTimeout;
        //
        // beforeEach(() => {
        //     originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        //     jasmine.DEFAULT_TIMEOUT_INTERVAL = Number.MAX_SAFE_INTEGER;
        // });
        //
        // afterEach(() => {
        //   jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        // });

        browser.driver.get("http://tutorialsninja.com/demo").then(() => {
            browser.driver.sleep(3000);
            let items = order_data["items"];
            for (let i in items) {
                // there could be some schema validation routine, but we just KISS
                if (items[i].prod_id) {
                    console.debug('Ordering ' + items[i].prod_name);
                    // debugger;
                    browser.driver.get("http://tutorialsninja.com/demo/index.php?route=product/product&product_id=" + items[i].prod_id).then(() => {
                        // debugger;
                        // if (items[i].prod_options) {
                        //     // iterate over product options by label name and set corresponding values
                        //     let opts = items[i]['prod_options'];
                        //     for (let oo in opts) {
                        //         // debugger;
                        //         console.debug(opts[oo]);
                        //     }
                        // }
                        browser.driver.sleep(3000);
                        browser.driver.findElement(By.xpath(ADD2CART_BTN_XPTH)).click().then(() => {
                            let prod_url = ADDED_MSG_XPTH.replace('{{product_id}}', items[i].prod_id);
                            browser.driver.sleep(3000);
                            let success = browser.driver.findElement(By.xpath(prod_url));
                            expect(until.elementIsVisible(success));
                        });
                    });
                }
                // else if (items[i].prod_title) {
                //     // debugger;
                //     let search_inp = browser.driver.findElement(By.xpath(SEARCH_XPTH));
                //     search_inp.sendKeys(items[i].prod_title + '\n').then(() => {
                //         // TODO: select first item from search results, then add to cart
                //     })
                // }
            }
        }).then(() => {
            // debugger;
            browser.driver.findElement(By.xpath(CHKOUT_LNK_XPTH)).click().then( () => {

                browser.driver.sleep(3000);
                browser.driver.getCurrentUrl().then((u) => {
                    if (u === 'http://tutorialsninja.com/demo/index.php?route=checkout/checkout') {
                        checkout_options();
                    }
                    else {
                        fail('Can\'t open Checkout page!!! No items in cart?')
                    }
                // }).then( () => {
                //     doneFn();
                });
            });
        });
    });

    let checkout_options = function () {
        console.debug('checkout_options');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(CHKOUT_1ST_STEP_XPATH)).getAttribute('id').then((id) => {
            if (id === 'collapse-checkout-option') {
                console.debug('Logging in as ' + auth_data.username);
                browser.driver.findElement(By.xpath(USERNAME_INPUT_XPTH)).sendKeys(auth_data.username).then(() => {
                    browser.driver.findElement(By.xpath(PASSWORD_INPUT_XPTH)).sendKeys(auth_data.password + '\n');
                    // .then( () => {
                    //   checkout_billing_addr();
                    // });
                });
            }
            // else {
            //     console.debug('Already logged in as' + auth_data.username);
            // }
        }).then(() => {
                checkout_billing_addr();
        });
    };

    let checkout_billing_addr = function () {
        console.debug('checkout_billing_addr');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(BILL_ADDR_EXIST_XPTH)).click().then( () => {
            // browser.driver.sleep(500);
            // browser.driver.findElement(By.xpath(BILL_ADDR_DD_XPTH)).click().then( () => {
                browser.driver.sleep(500);
                browser.driver.findElement(By.xpath(BILL_CONT_BTN_XPTH)).click().then( () => {
                    browser.driver.sleep(500);
                    checkout_delivery_addr();
                });
            // });
        });
    };

    let checkout_delivery_addr = function () {
        console.debug('checkout_delivery_addr');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(DEL_ADDR_EXIST_XPTH)).click().then( () => {
            // browser.driver.sleep(500);
            // browser.driver.findElement(By.xpath(DEL_ADDR_DD_XPTH)).click().then( () => {
                browser.driver.sleep(500);
                // debugger;
                browser.driver.findElement(By.xpath(DEL_CONT_BTN_XPTH)).click().then( () => {
                    browser.driver.sleep(500);
                    checkout_delivery_method();
                });
            // });
        });
    };

    let checkout_delivery_method = function () {
        console.debug('checkout_delivery_method');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(DEL_METH_EXIST_XPTH)).click().then( () => {
            browser.driver.sleep(500);
            browser.driver.findElement(By.xpath(DEL_METH_BTN_XPTH)).click().then( () => {
                browser.driver.sleep(500);
                checkout_payment_method();
            });
        });
    };

    let checkout_payment_method = function () {
        console.debug('checkout_payment_method');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(PAY_METH_EXIST_XPTH)).click().then( () => {
            browser.driver.sleep(500);
            browser.driver.findElement(By.xpath(PAY_METH_AGREE_XPTH)).click().then( () => {
                browser.driver.sleep(500);
                browser.driver.findElement(By.xpath(PAY_METH_BTN_XPTH)).click().then( () => {
                    browser.driver.sleep(500);
                    checkout_confirm();
                });
            });
        });
    };

    let checkout_confirm = function () {
        console.debug('checkout_confirm');
        browser.driver.sleep(3000);
        let calc_sub = 0;
        browser.driver.findElements(By.xpath(CONFIRM_ROW_PRICE_XPTH)).then((rows) => {
            browser.driver.sleep(500);
            rows.forEach((r) => {
                r.getText().then((t) => {
                 console.debug('found: ', t);
                 let price = parseFloat(t.replace('$','').replace(',',''));
                    console.debug('adding:', price);
                    calc_sub += price;
                });
            });
        }).then(() => {
            browser.driver.findElement(By.xpath(CONFIRM_ROW_SUBTT_XPTH)).getText().then((r) => {
                browser.driver.sleep(500);
                let sub_total = parseFloat(r.replace('$', '').replace(',', ''));
                console.debug('sub_total:', sub_total);
                // debugger;
                assert(sub_total === calc_sub);
                browser.driver.findElement(By.xpath(CONFIRM_ROW_SHPNG_XPTH)).getText().then((r) => {
                    browser.driver.sleep(500);
                    sub_total += parseFloat(r.replace('$', '').replace(',', ''));
                    browser.driver.findElement(By.xpath(CONFIRM_ROW_TOTAL_XPTH)).getText().then((r) => {
                        browser.driver.sleep(500);
                        let total = parseFloat(r.replace('$', '').replace(',', ''));
                        // debugger;
                        assert(sub_total === total);
                        browser.driver.findElement(By.xpath(CONFIRM_BTN_XPTH)).click().then(() => {
                            browser.driver.sleep(500);
                            checkout_done();
                        });
                    });
                });
            });
        });
    };

    let checkout_done = function () {
        console.debug('checkout_done');
        browser.driver.sleep(3000);
        browser.driver.findElement(By.xpath(ORDER_SUCSESS_MSG_XPTH)).getText().then((hooray) => {
            // debugger;
            expect(hooray).toBe('Your order has been placed!');
            // browser.driver.findElement(By.xpath(ORDER_SUCSESS_BTN_XPTH)).click().then(() => {
            //     browser.driver.sleep(500);
            //     console.debug('Your order has been placed');
            //     debugger;
            // });
        });
    };
});
